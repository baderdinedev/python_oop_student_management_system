from Student import Student
from School import School
class Main:
    def __init__(self):
        self.school = School()

    def display_menu(self):
        print("1. Add Student")
        print("2. Add Teacher")
        print("3. Add Course")
        print("4. Enroll Student")
        print("5. Add Grade")
        print("6. Exit")

    def run(self):
        while True:
            self.display_menu()
            choice = input("Enter your choice: ")
            if choice == "1":
                student_id = input("Enter student ID: ")
                name = input("Enter student name: ")
                age = input("Enter student age: ")
                gender = input("Enter student gender: ")
                grade_level = input("Enter student grade level: ")
                student = Student(student_id, name, age, gender, grade_level)
                self.school.add_student(student)
            elif choice == "2":
                teacher_id = input("Enter teacher ID: ")
                name = input("Enter teacher name: ")
                subjects = self.school.load_subjects()
                print("Available subjects:")
                for i, subject in enumerate(subjects, start=1):
                    print(f"{i}. {subject}")
                subject_choice = int(input("Select a subject (enter corresponding number): "))
                subject = subjects[subject_choice - 1]
                age = input("Enter teacher age: ")  # Add this line
                self.school.add_teacher(teacher_id, name, subject, age)  # Update this line

if __name__ == "__main__":
    app = Main()
    app.run()
