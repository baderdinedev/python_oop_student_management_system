from Enrollment import Enrollment
from Student import Student
from Teacher import Teacher
from Grade import Grade
import json
import os
class School:
    def __init__(self):
        self.students = []
        self.teachers = []
        self.courses = []
        self.enrollments = []
        self.grades = []
        self.data_folder = "data"

    def add_teacher(self, teacher_id, name, subject, age):
        teacher = Teacher(teacher_id, name, subject, age)
        self.teachers.append(teacher)
        self.save_teachers_to_file()

    def save_teachers_to_file(self):
        file_path = os.path.join(self.data_folder, "teachers.json")
        with open(file_path, "w") as json_file:
            json.dump([teacher.__dict__ for teacher in self.teachers], json_file, indent=4)

    def load_subjects(self):
        file_path = os.path.join(self.data_folder, "subjects.json")
        with open(file_path, "r") as json_file:
            return json.load(json_file)
        
    def add_course(self, course):
        self.courses.append(course)

    def enroll_student(self, student, course):
        enrollment = Enrollment(student, course)
        self.enrollments.append(enrollment)

    def add_grade(self, student, course, grade):
        grade_entry = Grade(student, course, grade)
        self.grades.append(grade_entry)