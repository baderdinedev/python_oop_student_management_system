class Student:
    def __init__(self, student_id, name, age, gender, grade_level):
        self.student_id = student_id
        self.name = name
        self.age = age
        self.gender = gender
        self.grade_level = grade_level
